package com.prueba.tecnica.controllers;

import com.prueba.tecnica.models.dto.ResponseDTO;
import com.prueba.tecnica.models.dto.TaskDTO;
import com.prueba.tecnica.models.entity.Task;
import com.prueba.tecnica.services.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

/**
 * Controlador con los servicios asociados a la gestión de las tareas.
 */
@CrossOrigin(allowedHeaders = "*")
@RestController
@RequestMapping("/api/tarea")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    /**
     * Servicio para crear una tarea.
     */
    @PostMapping("/create")
    public ResponseDTO<Task> createTask(@RequestBody TaskDTO tareaDTO) {
        return taskService.createTask(tareaDTO);
    }

    /**
     * Servicio para consultar el listdo de tareas asociadas a un usuario.
     */
    @GetMapping("/list")
    public ResponseDTO<Task> getTaskUser(@PathParam("idUsuario") Long idUsuario) {
        return taskService.getTasksUser(idUsuario);
    }

    /**
     * Servicio para consultar una tarea por Id.
     */
    @GetMapping("/{idTarea}")
    public ResponseDTO<Task> getTaskId(@PathVariable("idTarea") Long idTarea) {
        return taskService.getTaskId(idTarea);
    }

    /**
     * Servicio para actualizar una tarea.
     */
    @PutMapping("/update")
    public ResponseDTO<Task> updateTask(@RequestBody TaskDTO tareaDTO) {
        return taskService.updateTask(tareaDTO);
    }

    /**
     * Servicio para eliminar una tarea.
     */
    @DeleteMapping("/{idTarea}")
    public ResponseDTO deleteTask(@PathVariable("idTarea") Long idTarea) {
        return taskService.deleteTask(idTarea);
    }
}
