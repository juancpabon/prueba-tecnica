package com.prueba.tecnica.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * DTO para gestionar los servicios de las tareas.
 */
@Data
@ToString
@NoArgsConstructor
public class TaskDTO {
    private Long idTarea;
    private String nombre;
    private String descripcion;
    private Short estado;
    private Long idUsuario;
}
