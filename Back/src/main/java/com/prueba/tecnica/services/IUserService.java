package com.prueba.tecnica.services;

import com.prueba.tecnica.models.dto.RequestLoginDTO;
import com.prueba.tecnica.models.dto.ResponseDTO;
import com.prueba.tecnica.models.entity.User;

public interface IUserService {
    ResponseDTO<User> createUser(User user);
    ResponseDTO<User> loginUser(RequestLoginDTO request);
}
