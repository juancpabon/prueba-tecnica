export interface TaskForm {
    Serial: number,
    Name: string,
    Description: string,
    State: number,
    UserID: number,
}

export class TaskController {
    static async createTask(task: TaskForm) {
        const response = await fetch('http://localhost:8080/api/tarea/create', {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idTarea: task.Serial,
                nombre: task.Name,
                descripcion: task.Description,
                estado: task.State,
                idUsuario: task.UserID,
            })
        })
        return await response.json();
    }

    static async readAllTask(userID: number) {
        const response = await fetch('http://localhost:8080/api/tarea/list?idUsuario=' + userID, {
            method: "GET",
            mode: "cors",
            cache: 'default',
            headers: new Headers({
                'Content-Type': 'application/json',
            })
        })
        return await response.json();
    }

    static async readById() {
        const response = await fetch('http://localhost:8080/api/tarea/1', {
            method: "GET",
            mode: "cors"
        })
        return await response.json();
    }

    static async updateTask(task: TaskForm) {
        const response = await fetch('http://localhost:8080/api/tarea/update', {
            method: "PUT",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idTarea: task.Serial,
                nombre: task.Name,
                descripcion: task.Description,
                estado: task.State,
                idUsuario: task.UserID,
            })
        })
        return await response.json();
    }

    static async deleteTask(serial: number) {
        const response = await fetch('http://localhost:8080/api/tarea/' + serial, {
            method: "DELETE",
            mode: "cors",
        })
        return await response.json();
    }
}