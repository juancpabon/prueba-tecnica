import {Button, Col, Container, Dropdown, Form, Modal, Row} from "react-bootstrap";
import {BoxArrowRight} from "../component/icons/BoxArrowRight";
import {CardTask} from "../component/CardTask";
import React, {ReactNode, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {List} from "../component/icons/List";
import {TaskController} from "../controllers/TaskController";
import { Plus } from "../component/icons/Plus";

type Props = {
    onClick?: (event: any) => void
}

// Retorna un número aleatorio entre min (incluido) y max (excluido)
function getRandomArbitrary(min: number, max: number) {
    return Math.random() * (max - min) + min;
}

// The forwardRef is important!!
// Dropdown needs access to the DOM node in order to position the Menu
const CustomToggle = React.forwardRef(({onClick}: Props, ref: any) => (
    <span
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick?.(e);
        }}>
        <List size={16}/>
    </span>
));

export function Tasks() {
    const [update, setUpdate] = useState(true);

    const [name, setName] = useState('');
    const [nameTask, setNameTask] = useState('');
    const [descriptionTask, setDescriptionTask] = useState('');
    const [showModalCreation, setShowModalCreation] = useState(false);

    const [todoTask, setTodoTask] = useState<ReactNode[]>([]);
    const [doneTask, setDoneTask] = useState<ReactNode[]>([]);
    const [progressTask, setProgressTask] = useState<ReactNode[]>([]);

    const navigate = useNavigate();

    useEffect(() => {
        const first = sessionStorage.getItem('First');
        const last = sessionStorage.getItem('Last');
        const patronymic = sessionStorage.getItem('Patronymic');
        const matronymic = sessionStorage.getItem('Matronymic');

        setName((first ?? '') + (last ?? '') + ' ' + (patronymic ?? '') + (matronymic ?? ''))
    }, [])

    useEffect(() => {
        (async () => {
            if (update) {
                const response = await TaskController.readAllTask(
                    parseInt(sessionStorage.getItem('UserID') as string));

                if (response.codeStatus === 200) {
                    setTodoTask(getAllToDoTask(response.listData));
                    setDoneTask(getAllDoneTask(response.listData));
                    setProgressTask(getAllProgressTask(response.listData));
                }
                setUpdate(false);
            }
        })();
    }, [update])

    const handleCreateTask = async () => {
        const response = await TaskController.createTask({
            Description: descriptionTask,
            Name: nameTask,
            Serial: getRandomArbitrary(5, 999999999),
            State: 1,
            UserID: parseInt(sessionStorage.getItem('UserID') as string),
        });
        if (response.codeStatus === 200) {
            setUpdate(true);
            setShowModalCreation(false);
        } else {
            navigate('/');
        }
    }

    const handleLogOut = () => {
        sessionStorage.clear();
        navigate("/");
    }

    const getAllToDoTask = (payload: Array<any>): ReactNode[] => {
        return payload.filter(item => item.estado === 1).map(item =>
            <CardTask
                key={item.idTarea}  
                serial={item.idTarea}
                state={item.estado}
                userID={parseInt(sessionStorage.getItem('UserID') as string)}
                name={item.nombre}
                type="primary"
                description={item.descripcion}
                onUpdate={setUpdate}/>);
    }

    const getAllProgressTask = (payload: Array<any>): ReactNode[] => {
        return payload.filter(item => item.estado === 2).map(item =>
            <CardTask
                key={item.idTarea}
                serial={item.idTarea}
                state={item.estado}
                userID={parseInt(sessionStorage.getItem('UserID') as string)}
                name={item.nombre}
                type="primary"
                description={item.descripcion}
                onUpdate={setUpdate}/>);
    }

    const getAllDoneTask = (payload: Array<any>): ReactNode[] => {
        return payload.filter(item => item.estado === 3).map(item =>
            <CardTask
                key={item.idTarea}
                serial={item.idTarea}
                state={item.estado}
                userID={parseInt(sessionStorage.getItem('UserID') as string)}
                name={item.nombre}
                description={item.descripcion}
                type="success"
                onUpdate={setUpdate}/>);
    }

    return <Container fluid className={"h-100 m-0 p-0"}>
        <Row className={"shadow-sm py-2"} style={{backgroundColor: "#0C2D48", color: "#fff"}}>
            <h2 className="col-8 text-start px-5">Usuario: {name} </h2>
            <h2 className="col-4 text-end px-5" title="Cerrar sesión" onClick={handleLogOut} style={{cursor: 'pointer'}}>
                <span style={{fontSize: '16px'}}>Cerrar sesión<BoxArrowRight className={"ms-3 text-end"}/></span>
            </h2>            
        </Row>

        <Row className={"rounded row-cols-lg-3 row-cols-1 min-vh-100 mx-3 my-0 p-2"}>
            <Col className={"border rounded mb-3"} style={{backgroundColor: "#FFF"}}>
                <Row className="rounded p-2 text-white" style={{backgroundColor: '#68BBE3'}}>
                    <h5 className="col-10 text-start" style={{ cursor: 'pointer'}} onClick={() => setShowModalCreation(true)}>
                        Nuevas tareas
                        <span className="mx-2"><Plus /></span>
                    </h5>
                </Row>
                {todoTask}
            </Col>

            <Col className={"border rounded mb-3"} style={{backgroundColor: "#FFF"}}>
                <Row className="bg-primary rounded text-start p-2 text-white">
                    <h5>En progreso</h5>
                </Row>
                {progressTask}
            </Col>

            <Col className="border rounded mb-3" style={{backgroundColor: "#FFF"}}>
                <Row className="rounded text-start p-2 text-white" style={{backgroundColor: '#21B6A8'}}>
                    <h5>Terminadas</h5>
                </Row>
                {doneTask}
            </Col>
        </Row>

        <Modal show={showModalCreation} onHide={() => setShowModalCreation(false)} centered>
            <Modal.Header closeButton>
                <Modal.Title>Registro de tareas</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Nombre de la tarea:</Form.Label>
                        <Form.Control
                            onChange={({target}) => setNameTask(target.value)}
                            type="input"
                            autoComplete="none"
                            aria-autocomplete={"none"}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Descripción de la tarea:</Form.Label>
                        <Form.Control
                            onChange={({target}) => setDescriptionTask(target.value)}
                            as="textarea"
                            rows={3}/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => setShowModalCreation(false)}>
                    Cancelar
                </Button>
                <Button variant="primary" onClick={handleCreateTask}>
                    Crear tarea
                </Button>
            </Modal.Footer>
        </Modal>
    </Container>
}